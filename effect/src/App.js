import React, { useState } from 'react';
import FichaUsuario from './FichaUsuario'
import ListaUsuarios from './ListaUsuarios'
import './App.css';


const App = () => {
  const [uid, setUid] = useState(1)
  return (
    <>
      <div className="app">
        <ListaUsuarios />
        <div className="ficha-y-boton">
          <FichaUsuario id={uid} />
          <button onClick={() => setUid(uid === 10 ? 1 : uid + 1)}>
            Siguiente
        </button>
        </div>
      </div>
    </>

  )
}

export default App;
