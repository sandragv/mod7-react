import React, { useState, useEffect } from 'react';

const getUsers = (id) =>
  fetch(`https://jsonplaceholder.typicode.com/users/`)
    .then(r => r.json())

const ListaUsuarios = ({ id }) =>  {
    const [users, setUsers] = useState ()
    useEffect(() => {
        getUsers().then(u => setUsers (u))
    }, [id])

    return (
        <ul className="user-list">
            {users && users.map (u =>
                <li key= {u.id}>
                    {u.name}
                </li>
            )}
        </ul>
    )
}


export default ListaUsuarios