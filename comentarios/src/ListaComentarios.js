import React, { useState, useEffect } from 'react'

const getComments = () =>
    fetch('https://jsonplaceholder.typicode.com/comments')
        .then(r => r.json())

const ListaComentarios = ({ id, setId }) => {
    const [comments, setComments] = useState()
    useEffect(() => {
        getComments().then(u => setComments(u))
    }, [id])

    return (
        <ul className="comment-list">
            {comments && comments.map(u =>
                <li
                    key={u.id}
                    className={u.id === id ? 'active' : ''}
                    onClick={() => setId(u.id)} //setId viene del padre App (hacemos llamada a esa función)
                >
                    {u.body}
                </li>
            )}

        </ul>
    )
}

export default ListaComentarios