import React, { useState }  from 'react'
import './App.css';
import FichaComentarios from './FichaComentarios.js';
import ListaComentarios from './ListaComentarios'

function App() {
  const [pId, setPid] = useState(1)
  const handleClick = () => setPid(pId === 100 ? 1 : pId + 1)
  return (
    <div className="app">
      <ListaComentarios id={pId} setId={setPid}/>
      <div className="ficha-y-boton">
        <FichaComentarios postId={pId} />
        <button onClick={handleClick}>
          Siguiente
      </button>
      </div>
    </div>
  );
}

export default App;
