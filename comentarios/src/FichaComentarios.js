import React, { useState, useEffect } from 'react'

const getPost = postId =>
    fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
        .then(r => r.json())

const FichaComentarios = ({ postId }) => {
    const [comments, setComments] = useState()
    useEffect(() => {
        getPost(postId).then(u => setComments(u))
    }, [postId])

    return (
        <ul className="comments-post-lis">
            {comments && comments.map(u =>
                <li key={u.id}>
                    {u.body}
                </li>)}

        </ul>
    )
}


export default FichaComentarios