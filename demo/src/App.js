import React, { useState } from 'react'
import './App.css';

const NumberPicker = () => {
  const [value, setValue] = useState(0)
  const handlePlus = () => setValue(value + 1)
  const handleMinus = () => setValue(value - 1)

  return (
    <div className="counter">
      <div className="number">{value}</div>
      <button onClick={handlePlus}>+1</button>
      <button onClick={handleMinus}>-1</button>
    </div>
  )
}

//function BotonBoleano ({ verdadero: v }) {
//  if (v) {
//    return verdadero;
//  } return falso;
//}

const BotonCambia = () => {
  const [v, setV] = useState(true)

  const toggle = () => setV(!v)

  return (
    <button onClick={toggle} >
      {v ? 'si' : 'no'}
    </button>
  )
}

const BotonAcordeon = () => {
  const [visible, setVisible] = useState(true)

  const toggle = () => setVisible(!visible)

  return (
    <div className={'acordion' + (visible ? 'es-visible' : 'no-visible')}>
      <button onClick={toggle}> {visible ? 'Mostrar' : 'Ocultar '}

      </button>
      <div className="content">{visible ? 'Mostrar' : 'Ocultar '}</div>
    </div>
  )
}

const colores =['red', 'blue', 'green', 'yellow', 'black']

const BotonColores = () => {
  const [index, setIndex] = useState (0)
  const handleClick = () => setIndex (index == colores.length -1 ? 0 : index + 1 )
  const style = { backgroundColor: colores[index] }
  return (
    <div>
      Pulsa para cambiar de color: <button onClicK={handleClick}>Cambiar</button>
      <p>Color: {colores[index]}</p>
    </div>
  )
}




const App = () =>
  <div className="App">
    Selecciona un número: <NumberPicker />
    Verdadero o falso: <BotonCambia />
    <BotonAcordeon />
    <BotonColores />
  </div>

export default App
