import React from 'react';

const Address = ({ value, onChange, placeholder }) => {
    const handleStreet = e => {
        const newValue = {...value}
        newValue.street = e.target.value
        onChange(newValue)
    }
    const handleSuite = e => {
        const newValue = {...value}
        newValue.suite = e.target.value
        onChange(newValue)
    }
    const handleCity = e => {
        const newValue = {...value}
        newValue.city = e.target.value
        onChange(newValue)
    }
    const handleZipcode = e => {
        const newValue = {...value}
        newValue.zipcode = e.target.value
        onChange(newValue)
    }

    //const handleZipcode = e => onChange ({ ...value, zipcode: e.target.value }) con destructuring

    return (
        <div className="address">
            <input name="street" value={value.street} onChange={handleStreet} placeholder = "calle" />
            <input name="suite" value={value.suite} onChange={handleSuite} placeholder = "numero, piso" />
            <input name="city" value={value.city} onChange={handleCity} placeholder = "ciudad" />
            <input name="zipcode" value={value.zipcode} onChange={handleZipcode} placeholder = "codigo postal" />
        </div>
    )
}

export default Address;