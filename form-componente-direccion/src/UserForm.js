import React, { useState } from 'react';
import Address from './Address.js';
import Company from './Company.js';

const useFormField = (valorInicial) => {
    const [value, setValue] = useState(valorInicial || '')
    const handleChange = e => setValue(e.target.value)
    return [value, handleChange]
}

const UserForm = () => {
    const [name, handleName] = useFormField()
    const [surname, handleSurname] = useFormField()
    const [address, setAddress] = useState({ street: '', suite: '', city: '', zipcode: '' })
    const [email, handleEmail] = useFormField()
    const [phone, handlePhone] = useFormField()
    const [website, handleWebsite] = useFormField()
    const [company, setCompany] = useState ({ name: '', bs: ''})
    const [status, setStatus] = useState()

    const handleSubmit = async e => {
        e.preventDefault()
        const user= {name, surname, email, address}
        setStatus('loading')
        await fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'POST',
        body: JSON.stringify(user)
        })
        setStatus('ok!') //hacer algo más aqui. Enviar a otra ruta, etc
        //setTimeout(() => setStatus('ok'), 3000)
    }

    if (status === 'loading') {
        return (
            <p>Hola kease?</p>
        )
    }


    //Además del componente UserForms hay otro Address. No puede haber input para dirección porque es un objeto


    return (
        <form onSubmit={handleSubmit}>
            <input value={name} required onChange={handleName} placeholder="Nombre" />
            <input value={surname} required onChange={handleSurname} placeholder="Apellido" />
            <Address value={address} onChange={setAddress} placeholder="placeholder" />
            <input type="email" value={email} required onChange={handleEmail} placeholder="Email" />
            <input type="tel" value={phone} required onChange={handlePhone} placeholder="Teléfono" />
            <Company value={company} onChange={setCompany} placeholder="placeholder"/>
            <input type="url" value={website} required onChange={handleWebsite} placeholder="Web" />
            <button>Enviar</button>
            {status}
        </form>
    )

}

export default UserForm;