import React from 'react';

const Company = ({ value, onChange, placeholder }) => {
    const handleName = e => {
        const newValue = { ...value }
        newValue.name = e.target.value
        onChange(newValue)
    }
    const handleBs = e => {
        const newValue = { ...value }
        newValue.bs = e.target.value
        onChange(newValue)
    }


    return (
        <div className="company">
            <input name="name" value={value.name} onChange={handleName} placeholder="compañía" />
            <input name="bs" value={value.bs} onChange={handleBs} placeholder="bs" />
        </div>
    )
}

export default Company;