import React from 'react';
import UserForm from './UserForm.js';

import './App.css';

function App() {
  return (
    <div className="App">
      <UserForm />
    </div>
  );
}

export default App;
