import React, { useState } from 'react'

const Acordion = ({children}) => {
    const [visible, setVisible] = useState(true)

    const toggle = () => setVisible (!visible)

    return (
        <div className = {'acordion' + (visible ? 'visible' : 'invisible')}>
            <button onClick={toggle}>
                {visible ? 'ver menos...' : 'ver más...'}
            </button>
            {visible && <div className="content">{children}</div>}
        </div>
    )
}


export default Acordion;