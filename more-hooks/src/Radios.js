import React from 'react';

/*
* Mostrar un radio para cada opcion
* name: el nombre para todos los radios
* options: un array con los valores que se pueden escoger
* value: el valor marcado actualmente
* onChange: el manejador de evento change
**/

const Radios = ({ name, options, value, onChange }) => {
    return (
        <div>
            {options.map(option =>
                <label key={option}>
                    <input type="radio" name={name} value={option}
                        checked={value === option} onChange={onChange} />
                    {option}
                </label>
            )}

        </div>

    )
}

export default Radios;