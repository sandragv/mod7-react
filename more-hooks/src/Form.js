import React, { useState } from 'react';
import Radios from './Radios.js'

//custom hook that returns a handle
const useFormField = (valorInicial) => {
    const [value, setValue] = useState(valorInicial || '')
    const handleChange = e => setValue(e.target.value)
    return [value, handleChange]
}

const Form = () => {
    const [nombre, handleNombre] = useFormField()
    const [apellido, handleApellido] = useFormField()
    const [email, handleEmail] = useFormField()
    const [password, handlePassword] = useFormField()
    const [gender, handleGender] = useFormField()
    const [tier, handleTier] = useFormField()
    const [status, setStatus] = useState()

    //con checkbox no podemos usar nuestro custom hook porque necesita checked (es bolenao) y no value
    const [legal, setLegal] = useState(false)
    const handleLegal = e => setLegal(e.target.checked)

    const handleSubmit = e => {
        e.preventDefault()
        setStatus('loading')
        setTimeout(() => setStatus('ok'), 3000)
    }


    if (status === 'loading') {
        return (
            <p>Hola kease?</p>
        )
    }

    return (

        <form onSubmit={handleSubmit}>
            <label>
                Nombre
            <input value={nombre} required onChange={handleNombre} placeholder="Escribe tu nombre" />
            </label>
            <label>
                Apellido
            <input value={apellido} required onChange={handleApellido} placeholder="Escribe tu apellido" />
            </label>
            <Radios name="gender" options={["male", "female"]} value={gender} onChange={handleGender} />
            <Radios name="tier" options={["free", "cheap", "premium"]} value={tier} onChange={handleTier} />
            < label >
                Email
            < input type="email" value={email} required onChange={handleEmail} placeholder="Escribe tu email" />
            </label>
            <label>
                Contraseña
            <input type="password" value={password} required onChange={handlePassword} placeholder="Tu contraseña" />
            </label>
            <label>
            <input type="checkbox" checked={legal} onChange={handleLegal} />
                Acepto términos y condiciones
            </label>
            <button>Enviar</button>
            {status}
        </form>
    );
}


export default Form;