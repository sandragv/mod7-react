import React from 'react'
import { Link } from "react-router-dom"
import ChatEntry from './ChatEntry.js'
import './ChatList.css'

const ChatList = ({ chats }) =>
    <main className="chat-list" to='/'>
        {chats && chats.map(chat =>
            <ChatEntry key={chat.name} chat={chat} />
        )}
    </main>

export default ChatList
